# documentación de notas importantes
## comandos git
- regresar los cambios a un commit especifico
git reset --hard 4146aa3

- restablecimiento completo del repositorio remoto
NOTA: Una vez que haya restablecido el repositorio local, simplemente haga un empuje forzado
git push -f origin main

#ejemplo de código
```
<div class="text-center">
    <h2 class="section-heading text-uppercase">Servicios</h2>
    <h3 class="section-subheading text-muted">Es un gusto poder atenderte y brindarte nuestros servicios.</h3>
</div>
```

